// copyami copies an Amazon Machine Image (AMI) from one AWS account to
// another. It also copies resource tags, making tag-based lookups work across
// AWS accounts.
package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sort"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"github.com/aws/aws-sdk-go-v2/service/sts"
)

const (
	egCommand      = "examples"
	lsCommand      = "ls"
	copyAMICommand = "cp"

	helpArg = "h"

	lsAllImagesArg = "a"

	appName = "copyami"
	usage   = appName + `
Copy an Amazon Machine Image (AMI) from one AWS account to another.

usage: ` + appName + ` [primary-options] <command> [command-options]

shared environment variables:
  '` + regionEnv + `'

source account environment variables:
  '` + srcKeyIDEnv + `', '` + srcKeyEnv + `', '[` + srcKeyToken + `]'

destination account environment variables:
  '` + destKeyIDEnv + `', '` + destKeyEnv + `', '[` + destTokenEnv + `]'

primary options:
`

	examples = `usage examples:

> Set environment variables for source account commands:
export ` + regionEnv + `=us-east-1
read -s ` + srcKeyIDEnv + `
# Enter the source account's AWS IAM key ID and hit 'enter'.
export ` + srcKeyIDEnv + `
read -s ` + srcKeyEnv + `
# Enter the source account's AWS IAM secret key and hit 'enter'.
export ` + srcKeyEnv + `
# Optional: AWS session token:
# read -s ` + srcKeyToken + `
# export ` + srcKeyToken + `

> Set environment variables for destination account commands:
export ` + regionEnv + `=us-east-1
read -s ` + destKeyIDEnv + `
# Enter the destination account's AWS IAM key ID and hit 'enter'.
export ` + destKeyIDEnv + `
read -s ` + destKeyEnv + `
# Enter the destination's' account's AWS IAM secret key and hit 'enter'.
export ` + destKeyEnv + `
# Optional: Enter the destination's account's AWS IAM token.
read -s ` + destTokenEnv + `
export ` + destTokenEnv + `

> List EC2 image builder AMIs in the source account:
` + appName + ` ` + lsCommand + `

> List *all* AMIs in the source account:
` + appName + ` ` + lsCommand + ` -` + lsAllImagesArg + `

> Copy the specified AMI in the source account to the destination account:
` + appName + ` ` + copyAMICommand + ` ami-abcdefg
`

	createdByImageBuilderTagName = "Ec2ImageBuilderArn"

	regionEnv    = "AWS_DEFAULT_REGION"
	srcKeyIDEnv  = "AWS_ACCESS_KEY_ID"
	srcKeyEnv    = "AWS_SECRET_ACCESS_KEY"
	srcKeyToken  = "AWS_SESSION_TOKEN"
	destKeyIDEnv = "DEST_AWS_ID"
	destKeyEnv   = "DEST_AWS_KEY"
	destTokenEnv = "DEST_AWS_TOKEN"
)

func main() {
	help := flag.Bool(
		helpArg,
		false,
		"Display this information")

	listAMIsFS := flag.NewFlagSet(lsCommand, flag.ExitOnError)
	listAMIsFlags := &listAMIsFlagSet{
		fs:      listAMIsFS,
		allAMIs: listAMIsFS.Bool(lsAllImagesArg, false, "Include AMIs created outside of EC2 ImageBuilder"),
	}

	copyAMIFs := flag.NewFlagSet(copyAMICommand, flag.ExitOnError)
	copyAmiFlags := &copyAMIFlagSet{
		flagSet: copyAMIFs,
	}

	flag.Parse()

	if *help {
		_, _ = os.Stderr.WriteString(usage)
		flag.PrintDefaults()

		_, _ = os.Stderr.WriteString("\ncommands:\n")
		_, _ = os.Stderr.WriteString("\n" + egCommand + ": Display detailed usage examples.\n")
		_, _ = os.Stderr.WriteString("\n" + lsCommand + ": List AMIs in the current account.\n")
		listAMIsFS.PrintDefaults()
		_, _ = os.Stderr.WriteString("\n" + copyAMICommand + ": Copy an AMI to the destination account.\n")
		copyAMIFs.PrintDefaults()
		os.Exit(1)
	}

	if flag.NArg() < 1 {
		log.Fatalf("please specify one of the following commands:\n'%s'",
			strings.Join([]string{egCommand, lsCommand, copyAMICommand}, "', '"))
	}

	if flag.Arg(0) == egCommand {
		// The example command logic could go in the switch statement,
		// but then we do things with the AWS library, and check stuff
		// (like env. variables) that are needed for other commands,
		// but not examples.
		_, _ = os.Stderr.WriteString(examples)
		os.Exit(0)
	}

	region, isSet := os.LookupEnv(regionEnv)
	if !isSet {
		log.Fatalf("please set the '%s' environment variable", regionEnv)
	}

	sourceAWSConfig := aws.Config{
		Region:      region,
		Credentials: credentials.StaticCredentialsProvider{Value: aws.Credentials{
			AccessKeyID:     os.Getenv(srcKeyIDEnv),
			SecretAccessKey: os.Getenv(srcKeyEnv),
			SessionToken:    os.Getenv(srcKeyToken),
		}},
	}

	srcECClient := ec2.NewFromConfig(sourceAWSConfig)

	switch command := flag.Arg(0); command {
	case lsCommand:
		srcSTSClient := sts.NewFromConfig(sourceAWSConfig)
		srcSTSIDOut, err := srcSTSClient.GetCallerIdentity(context.TODO(), &sts.GetCallerIdentityInput{})
		if err != nil {
			log.Fatalf("failed to get sts caller identity - %s", err)
		}

		err = listAMIs(listAMIsConfig{
			ec2Client: srcECClient,
			accountID: srcSTSIDOut.Account,
			flags:     listAMIsFlags,
		})
		if err != nil {
			log.Fatalln(err)
		}
	case copyAMICommand:
		destAWSConfig := aws.Config{
			Region:      region,
			Credentials: credentials.StaticCredentialsProvider{Value: aws.Credentials{
				AccessKeyID:     mustLookupEnv(destKeyIDEnv),
				SecretAccessKey: mustLookupEnv(destKeyEnv),
				SessionToken:    os.Getenv(destTokenEnv),
			}},
		}

		dstSTSClient := sts.NewFromConfig(destAWSConfig)
		dstSTSIDOut, err := dstSTSClient.GetCallerIdentity(context.TODO(), &sts.GetCallerIdentityInput{})
		if err != nil {
			log.Fatalf("failed to get sts caller identity - %s", err)
		}

		err = copyAMI(copyAMIConfig{
			srcEC2Client: srcECClient,
			dstEC2Client: ec2.NewFromConfig(destAWSConfig),
			srcRegion:    region,
			dstRegion:    region,
			toAccountID:  dstSTSIDOut.Account,
			flags:        copyAmiFlags,
		})
		if err != nil {
			log.Fatalln(err)
		}
	default:
		log.Fatalf("unknown command: '%s'", command)
	}
}

func mustLookupEnv(env string) string {
	val, isSet := os.LookupEnv(env)
	if !isSet {
		log.Fatalf("please set the '%s' environment variable", env)
	}

	return val
}

type listAMIsConfig struct {
	ec2Client *ec2.Client
	accountID *string
	flags     *listAMIsFlagSet
}

type listAMIsFlagSet struct {
	fs      *flag.FlagSet
	allAMIs *bool
}

func listAMIs(config listAMIsConfig) error {
	err := config.flags.fs.Parse(flag.Args()[1:])
	if err != nil {
		return err
	}

	input := &ec2.DescribeImagesInput{
		Filters: []types.Filter{
			{
				Name:   aws.String("owner-id"),
				Values: []string{
					*config.accountID,
				},
			},
		},
	}

	if !*config.flags.allAMIs {
		input.Filters = append(input.Filters, types.Filter{
			Name:   aws.String("tag:CreatedBy"),
			Values: []string{"EC2 Image Builder"},
		})
	}

	images, err := config.ec2Client.DescribeImages(context.TODO(), input)
	if err != nil {
		return err
	}

	var amis []ami
	for _, image := range images.Images {
		a := ami{
			ID: *image.ImageId,
		}
		if image.Name != nil {
			a.Name = *image.Name
		}

		var ibInfo *ImageBuilderAMIInfo
		for _, t := range image.Tags {
			if *t.Key != createdByImageBuilderTagName {
				continue
			}

			ibInfo, err = parseImageBuilderAMIArn(*t.Value)
			if err != nil {
				return fmt.Errorf("failed to parse image builder arn for '%s' - %w",
					*image.ImageId, err)
			}

			if !*config.flags.allAMIs {
				a.Name = ibInfo.ImageName
			}

			break
		}

		if !*config.flags.allAMIs && ibInfo == nil {
			continue
		}
		a.ImageBuilderInfo = ibInfo
		amis = append(amis, a)
	}

	sort.Slice(amis, func(i, j int) bool {
		return amis[i].Name < amis[j].Name
	})

	for i := range amis {
		_, _ = os.Stdout.WriteString(amis[i].PrettyString())
		_, _ = os.Stdout.WriteString("\n")
	}

	return nil
}

// arn:aws:imagebuilder:us-east-1:<accound-id>:image/lnk360-qa-docker/0.1.8/1
func parseImageBuilderAMIArn(arn string) (*ImageBuilderAMIInfo, error) {
	const match = ":image/"
	index := strings.Index(arn, match)
	if index < 0 {
		return nil, fmt.Errorf("failed to find image build info index in arn '%s'", arn)
	}

	var info ImageBuilderAMIInfo
	rem := arn[index+len(match):]
	for i, s := range strings.Split(rem, "/") {
		switch i {
		case 0:
			info.ImageName = s
		case 1:
			info.RecipeVersion = s
		case 2:
			info.BuildNumber = s
		}
	}

	return &info, nil
}

type ami struct {
	ID               string
	Name             string
	ImageBuilderInfo *ImageBuilderAMIInfo
}

func (o ami) PrettyString() string {
	sb := strings.Builder{}
	sb.WriteString(o.ID)
	sb.WriteString(" | ")
	if o.ImageBuilderInfo == nil {
		sb.WriteString("'")
		sb.WriteString(o.Name)
		sb.WriteString("'")
	} else {
		sb.WriteString(o.ImageBuilderInfo.ImageName)
		sb.WriteString(" | v: '")
		sb.WriteString(o.ImageBuilderInfo.RecipeVersion)
		sb.WriteString("' b: '")
		sb.WriteString(o.ImageBuilderInfo.BuildNumber)
		sb.WriteString("'")
	}

	return sb.String()
}

type ImageBuilderAMIInfo struct {
	ImageName     string
	RecipeVersion string
	BuildNumber   string
}

type copyAMIConfig struct {
	srcEC2Client *ec2.Client
	dstEC2Client *ec2.Client
	srcRegion    string
	dstRegion    string
	toAccountID  *string
	flags        *copyAMIFlagSet
}

type copyAMIFlagSet struct {
	flagSet *flag.FlagSet
}

func copyAMI(config copyAMIConfig) error {
	err := config.flags.flagSet.Parse(flag.Args()[1:])
	if err != nil {
		return err
	}

	if config.flags.flagSet.NArg() != 1 {
		return errors.New("please specify an ami ID to copy")
	}

	imageID := config.flags.flagSet.Arg(0)

	src, err := config.srcEC2Client.DescribeImages(context.TODO(), &ec2.DescribeImagesInput{
		ImageIds: []string{imageID},
	})
	if err != nil {
		return err
	}

	if len(src.Images) != 1 {
		return fmt.Errorf("got %d amis in source image lookup - expected only one image", len(src.Images))
	}

	srcImageTags, err := config.srcEC2Client.DescribeTags(context.TODO(), &ec2.DescribeTagsInput{
		Filters: []types.Filter{
			{
				Name:   aws.String("resource-id"),
				Values: []string{imageID},
			},
		},
	})
	if err != nil {
		return err
	}

	var volumes []*string
	for _, mapping := range src.Images[0].BlockDeviceMappings {
		if mapping.Ebs != nil && mapping.Ebs.SnapshotId != nil {
			volumes = append(volumes, mapping.Ebs.SnapshotId)
		}
	}

	err = shareOrUnshareAMI(shareOrUnshareAMIConfig{
		ec2Client:     config.srcEC2Client,
		imageID:       imageID,
		ebsVolumes:    volumes,
		targetAccount: config.toAccountID,
		share:         true,
	})
	if err != nil {
		return err
	}
	defer func() {
		err := shareOrUnshareAMI(shareOrUnshareAMIConfig{
			ec2Client:     config.srcEC2Client,
			imageID:       imageID,
			ebsVolumes:    volumes,
			targetAccount: config.toAccountID,
			share:         false,
		})
		if err != nil {
			log.Fatalf("failed to unshare source ami - %s", err)
		}
	}()

	dst, err := config.srcEC2Client.DescribeImages(context.TODO(), &ec2.DescribeImagesInput{
		ImageIds: []string{imageID},
	})
	if err != nil {
		return err
	}

	if len(dst.Images) != 1 {
		return fmt.Errorf("got %d amis in dest image lookup - expected only one image", len(src.Images))
	}

	log.Println("copying image...")

	copyName := src.Images[0].Name
	copyOut, err := config.dstEC2Client.CopyImage(context.TODO(), &ec2.CopyImageInput{
		Name:          copyName,
		SourceImageId: dst.Images[0].ImageId,
		SourceRegion:  aws.String(config.srcRegion),
		Description:   src.Images[0].Description,
	})
	if err != nil {
		return err
	}

	err = waitForAMICopy(waitForAMICopyConfig{
		dstEC2Client: config.dstEC2Client,
		imageID:      copyOut.ImageId,
	})
	if err != nil {
		return err
	}

	log.Printf("copy complete - new ami id: '%s' | name: '%s'", *copyOut.ImageId, *copyName)

	imageTags := make([]types.Tag, len(srcImageTags.Tags))
	for i, srcTag := range srcImageTags.Tags {
		imageTags[i] = types.Tag{
			Key:   srcTag.Key,
			Value: srcTag.Value,
		}
	}

	_, err = config.dstEC2Client.CreateTags(context.TODO(), &ec2.CreateTagsInput{
		Resources: []string{*copyOut.ImageId},
		Tags:      imageTags,
	})
	if err != nil {
		return err
	}

	return nil
}

type shareOrUnshareAMIConfig struct {
	ec2Client     *ec2.Client
	imageID       string
	ebsVolumes    []*string
	targetAccount *string
	share         bool
}

func shareOrUnshareAMI(config shareOrUnshareAMIConfig) error {
	createVolPermMods := &types.CreateVolumePermissionModifications{}
	createVolsPerm := []types.CreateVolumePermission{{UserId: config.targetAccount}}
	if config.share {
		createVolPermMods.Add = createVolsPerm
	} else {
		createVolPermMods.Remove = createVolsPerm
	}

	for _, ebs := range config.ebsVolumes {
		_, err := config.ec2Client.ModifySnapshotAttribute(context.TODO(), &ec2.ModifySnapshotAttributeInput{
			SnapshotId:             ebs,
			CreateVolumePermission: createVolPermMods,
		})
		if err != nil {
			return err
		}
	}

	lpMods := &types.LaunchPermissionModifications{}
	lp := []types.LaunchPermission{{UserId: config.targetAccount}}
	if config.share {
		lpMods.Add = lp
	} else {
		lpMods.Remove = lp
	}
	_, err := config.ec2Client.ModifyImageAttribute(context.TODO(), &ec2.ModifyImageAttributeInput{
		ImageId:          aws.String(config.imageID),
		LaunchPermission: lpMods,
	})
	if err != nil {
		return err
	}

	return nil
}

type waitForAMICopyConfig struct {
	dstEC2Client *ec2.Client
	imageID      *string
}

func waitForAMICopy(config waitForAMICopyConfig) error {
	dii := &ec2.DescribeImagesInput{
		ImageIds: []string{*config.imageID},
	}

	ticker := time.NewTicker(15*time.Second)
	defer ticker.Stop()

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, os.Kill)
	defer signal.Stop(signals)

	for {
		select {
		case <-ticker.C:
			out, err := config.dstEC2Client.DescribeImages(context.TODO(), dii)
			if err != nil {
				return err
			}

			if len(out.Images) != 1 {
				return fmt.Errorf("got %d amis in source image lookup - expected only one image", len(out.Images))
			}

			if out.Images[0].State == types.ImageStateAvailable {
				return nil
			}

			log.Printf("image copy status: '%s'", out.Images[0].State)
		case <-signals:
			return errors.New("canceled by user")
		}
	}
}
