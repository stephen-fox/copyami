# copyami
copyami copies an Amazon Machine Image (AMI) from one AWS account to
another. It also copies resource tags, making tag-based lookups work across
AWS accounts.

The copy operation consists of sharing the AMI and its snapshot volume(s)
with the destination account, making a copy of the AMI in the target account,
and then unsharing any resources that were required for the copy operation.

## Table of contents
[[_TOC_]]

## Usage
`copyami` implements commands (similar to `git`) that modify its behavior.

#### `examples`
Display detailed usage examples:

```sh
copyami examples
```

#### `ls`
List AMIs in the source account.

Note: This command requires the standard AWS environment variables:
- `AWS_DEFAULT_REGION`
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- Optional: `AWS_SESSION_TOKEN`

```sh
# Note: This looks for images built with EC2 ImageBuilder. To show all AMIs,
# specify 'ls -a'.
copyami ls
```

#### `cp`
Copy an AMI (by ID) from the source account into the destination account.

Note: This command requires the following environment variables:

Source account:
- `AWS_DEFAULT_REGION`
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- Optional: `AWS_SESSION_TOKEN`

Destination account:
- `DEST_AWS_ID`
- `DEST_AWS_KEY`
- Optional: `DEST_AWS_TOKEN`

```sh
copyami cp ami-abcdefg
```

## Installation

```sh
# Note: This downloads, compiles, and copies an executable into
# "~/go/bin/". You may need to add the directory to your PATH.
go install gitlab.com/stephen-fox/copyami/cmd/copyami@latest
```

## Building and running

Compile into an executable file:

```sh
go build cmd/copyami/main.go
```

Compile and run the application:

```sh
go run cmd/copyami/main.go
```
